import colors from 'colors';
import dotenv from 'dotenv';
import express from 'express';
import connectDB from './config/db.js';
import { errorHandler } from './middlewares/errorMiddleware.js';
import userRoutes from './routes/userRoutes.js';
import blogRoutes from './routes/blogRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import path from 'path';

dotenv.config();

connectDB();

const app = express();
app.use(express.json()); // Request Body Parsing

app.use('/api/blogs', blogRoutes);
app.use('/api/users', userRoutes);
app.use('/api/uploads', uploadRoutes);

const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

// If the environment is production, do not serve frontend files
if (process.env.NODE_ENV === 'production') {
  app.get('/', (req, res) => {
    res.send('API is running...');
  });
}

// Custom error handler
app.use(errorHandler);

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
  console.log(`Server running in ${process.env.NODE_ENV} mode, on port ${PORT}.`.yellow.bold);
});
