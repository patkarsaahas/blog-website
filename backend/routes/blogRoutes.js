import express from 'express';
import { getBlogs, getBlogById, deleteblog, updateBlog, createBlogComment, createBlog } from '../controllers/blogController.js';
import { protect } from '../middlewares/authMiddleware.js';
const router = express.Router();

router.route('/').get(getBlogs).post(protect,createBlog);

// router.route('/myblogs').get(getMyBlogs);

router.route('/:id/comments').post(protect, createBlogComment);

router.route('/:id').get(getBlogById).delete(protect, deleteblog).put(protect, updateBlog);



export default router;
