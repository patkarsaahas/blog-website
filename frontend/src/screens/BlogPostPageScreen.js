import React, { useEffect, useState } from 'react'
import { Box, Button, Flex, FormControl, FormLabel, Heading, Image, Text, Textarea } from '@chakra-ui/react'
import Message from '../components/Message';
import Loader from '../components/Loader';
import { createBlogComment, listBlogDetails } from '../actions/blogActions';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Link as RouterLink } from 'react-router-dom';
import '../css/style.css'
import { BLOG_COMMENT_CREATE_RESET } from '../constants/blogConstants';
const BlogPostPageScreen = () => {


    const { id } = useParams();
    const dispatch = useDispatch();

    const [comment, setComment] = useState('')

    const blogDetails = useSelector((state) => state.blogDetails);

    const { loading, error, blog } = blogDetails;

    const userLogin = useSelector((state) => state.userLogin);
    const { userInfo } = userLogin;

    const blogCommentCreate = useSelector((state) => state.blogCommentCreate);


    const { success: successBlogComment, error: errorBlogComment } =
        blogCommentCreate;

    useEffect(() => {
        if (successBlogComment) {
            alert('Comment submitted');
            setComment('');
            dispatch({ type: BLOG_COMMENT_CREATE_RESET })
        }
        dispatch(listBlogDetails((id)));
    }, [id, dispatch, successBlogComment]);

    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(createBlogComment(id, { comment }));
    };
    

    return (
        <>

            <Flex mb='5' gap='5'>
                <Button as={RouterLink} to='/' colorScheme='gray'>
                    Go Back
                </Button>
                <Button colorScheme='blue' id='downloadbtn' onClick={(e) => {window.print(e)}}>Download</Button>
            </Flex>
            {loading ? (
                <Loader />
            ) : error ? (
                <Message type='error'>{error}</Message>
            ) :
                (<>
                    <Box>
                        <Flex justifyContent='center' alignItems='center'>
                            <Box>
                                <Heading fontSize={{ base: '13px', md: '2rem' }} mt='1rem' mb='1rem' textAlign={{ base: 'center' }}>
                                    {blog.title}
                                </Heading>

                                <Flex justifyContent='center'>
                                    <Image src={blog.image} alt='blog post' height={{ base: 'auto', md: '400px' }} mt='1rem' mb='1rem' />
                                </Flex>
                                <Box mb='1rem'>
                                    <span style={{ marginLeft: '6rem' }} > <em>Author  {blog.author}</em>

                                    </span>
                                    <span className='posted'> <em>Posted   {blog.date}</em>

                                    </span>
                                </Box>

                            </Box>
                        </Flex>
                        <pre style={{ fontSize: 'lg', width: '80%', margin: 'auto', textAlign: 'center', whiteSpace: 'pre-wrap' }} >
                            {blog.content}
                        </pre>

                        {/* <Box textAlign='center' mt='5rem'>
                     <Comments />
                        </Box> */}
                    </Box>




                    <Box>

                        {errorBlogComment && (
                            <Message type='error'>{errorBlogComment}</Message>
                        )}


                        {userInfo ? (
                            <form onSubmit={submitHandler}>

                                <FormControl id='comment' mb='3'>
                                    <FormLabel>Comment</FormLabel>
                                    <Textarea
                                        value={comment}
                                        onChange={(e) => setComment(e.target.value)} border='1px solid black' _hover={{ border: '1px solid black' }}></Textarea>
                                </FormControl>

                                <Button colorScheme='teal' type='submit'>
                                    Post Comment
                                </Button>
                            </form>
                        ) : (
                            <Message>Please login to write a review</Message>
                        )}

                    </Box>

                    {blog?.comments?.map((comment) => (
                        <Box p='4' bgColor='white' rounded='md' mb='1' mt='5' height='3rem'>
                            <Text fontSize='xl'>{comment.comment}</Text>

                        </Box>
                    ))}

                </>
                )
            }
        </>
    )
}

export default BlogPostPageScreen;